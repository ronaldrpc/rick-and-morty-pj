import { Banner } from "../components/Banner";
import { Search } from "../components/Search";
import { CharacterList } from "../components/CharacterList";
import { useState } from "react";

export function Home() {
  const [search, setSearch] = useState("");

  console.log(search);

  return (
    <div>
      <Banner />
      <div className="px-3 py-5">
        <div className="row py-3">
          <div className="col">
            <h1>Characters</h1>
          </div>
          <div className="col-3 py-1">
            <Search search={search} toSearch={setSearch} />
          </div>
        </div>
        <div>
          <CharacterList filter={search} />
        </div>
      </div>
    </div>
  );
}
