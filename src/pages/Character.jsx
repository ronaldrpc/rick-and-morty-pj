import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import axios from "axios";
import { CharacterItem } from "../components/CharacterItem";
import { EpisodeList } from "../components/EpisodeList";

export function Character() {
  let { characterId } = useParams();
  const [character, setCharacter] = useState(null);
  const [episodes, setEpisodes] = useState(null);

  useEffect(() => {
    axios
      .get(`https://rickandmortyapi.com/api/character/${characterId}`)
      .then((response) => {
        setCharacter(response.data);
      });
  }, []);

  useEffect(() => {
    if (character) {
      let episodePromises = character.episode.map((ep) => axios.get(ep));
      Promise.all(episodePromises).then((responses) => setEpisodes(responses));
    }
  }, [character]);

  return (
    <div>
      {character ? (
        <div className="px-3">
          <div className="py-5 d-flex justify-content-center">
            <CharacterItem {...character} />
          </div>
          <h3>Episode List</h3>
          {episodes ? (
            <EpisodeList episodes={episodes} />
          ) : (
            "Loading episodes..."
          )}
        </div>
      ) : (
        ""
      )}
    </div>
  );
}
