import bannerURL from "../assets/img/banner-img.png";

export function Banner() {
  return (
    <div>
      <img
        style={{ height: "740px", objectFit: "cover" }}
        src={bannerURL}
        className="card-img"
        alt="banner"
      />
    </div>
  );
}
