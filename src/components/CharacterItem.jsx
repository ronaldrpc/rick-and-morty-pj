import { Link } from "react-router-dom";

export function CharacterItem({
  id,
  name,
  status,
  species,
  location,
  image,
  origin,
}) {
  return (
    <div className="col-4">
      <Link
        to={`/character/${id}`}
        style={{ textDecoration: "none", color: "initial" }}
      >
        <div className="card mb-3" style={{ maxWidth: "540px" }}>
          <div className="row g-0">
            <div className="col-md-4">
              <img
                src={image}
                className="img-fluid rounded-start"
                alt={name}
                style={{ height: "100%", objectFit: "cover" }}
              />
            </div>
            <div className="col-md-8">
              <div className="card-body">
                <h5 className="card-title">{name}</h5>
                <p className="card-text">
                  {status} - {species}
                </p>
                <p className="card-text text-muted mb-0">
                  Last known location:
                </p>
                <small className="text-muted">{location?.name}</small>
                <p className="card-text text-muted mb-0 mt-3">Origin:</p>
                <small className="text-muted">{origin?.name}</small>
              </div>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
}
