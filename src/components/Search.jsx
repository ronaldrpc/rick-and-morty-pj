export function Search({ search, toSearch }) {
  return (
    <div className="col">
      <input
        className="form-control mr-sm-2"
        type="text"
        placeholder="Search character"
        value={search}
        onChange={(event) => toSearch(event.target.value)}
      />
    </div>
  );
}
