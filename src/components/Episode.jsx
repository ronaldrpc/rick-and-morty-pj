export function Episode({ name, air_date, episode }) {
  return (
    <div className="col-3">
      <div className="card mb-3">
        <div className="card-body">
          <h5 className="card-title text-center">{name}</h5>
          <p className="card-text text-muted mb-0">Air date: {air_date}</p>
          <p className="card-text text-muted mb-0 mt-3">Episode: {episode}</p>
        </div>
      </div>
    </div>
  );
}
