export function Footer() {
  return (
    <div
      className="navbar-light fixed-bottom"
      style={{ backgroundColor: "#e3f2fd" }}
    >
      <p className="text-center py-2 mb-0">Proyecto creado por mí</p>
    </div>
  );
}
