import { Episode } from "./Episode";

export function EpisodeList({ episodes }) {
  return (
    <div className="row">
      {episodes.map((ep) => {
        return <Episode key={ep.data.id} {...ep.data} />;
      })}
    </div>
  );
}
