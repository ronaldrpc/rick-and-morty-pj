import { CharacterItem } from "./CharacterItem";
import { useState, useEffect } from "react";
import axios from "axios";

export function CharacterList({ filter }) {
  const [characters, setCharacters] = useState(null);
  const [filteredResults, setFilteredResults] = useState(null);

  function filterCharactersByName(chars) {
    if (filter && characters) {
      let filteredData = chars.results.filter((char) => {
        return char.name.toLowerCase().includes(filter.toLowerCase());
      });
      setFilteredResults(filteredData);
    } else {
      setFilteredResults(chars.results);
    }
  }

  useEffect(() => {
    axios.get("https://rickandmortyapi.com/api/character").then((response) => {
      setCharacters(response.data);
      filterCharactersByName(response.data);
    });
  }, [filter]);

  return (
    <div className="row">
      {filteredResults
        ? filteredResults.map((char) => {
            return <CharacterItem key={char.name} {...char} />;
          })
        : "Loading..."}
    </div>
  );
}
