import { Link } from "react-router-dom";

export function Navbar() {
  return (
    <nav
      className="navbar navbar-expand-lg navbar-light"
      style={{ backgroundColor: "#e3f2fd" }}
    >
      <Link className="navbar-brand px-3" to="/">
        Rick and Morty
      </Link>

      <div className="collapse navbar-collapse" id="navbarText">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            {/* <Link className="nav-link" to="/character">
              Characters
            </Link> */}
          </li>
        </ul>
      </div>
    </nav>
  );
}
